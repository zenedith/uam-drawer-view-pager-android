package com.example.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

public class RecipeDetailsFragment extends Fragment {

    private Recipe recipe;

    public RecipeDetailsFragment() {
        // nop
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recipe_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Button button = (Button) getView().findViewById(R.id.button);
        final ScrollView sv = (ScrollView) getView().findViewById(R.id.scroll);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sv.scrollTo(0,0);
            }
        });
        if (recipe != null) {
            showRecipe(recipe);
        }
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.details_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.save_to_disc: {
                saveRecipeToDisk(recipe);
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }

    }

    public void showRecipe(Recipe recipe) {
        this.recipe = recipe;
        TextView title = (TextView) getView().findViewById(R.id.title);
        TextView ingredients = (TextView) getView().findViewById(R.id.ingredients);
        TextView recipeText = (TextView) getView().findViewById(R.id.recipe);
        ImageView photo = (ImageView) getView().findViewById(R.id.photo);

        title.setText(recipe.getDishName());
        ingredients.setText(recipe.getIngredients());
        recipeText.setText(recipe.getRecipeText());
        photo.setImageResource(R.drawable.recipe4);
    }

    private void saveRecipeToDisk(Recipe recipe) {
        SaveRecipeOnDiskDelegate delegate = new SaveRecipeOnDiskDelegate();
        delegate.saveRecipe(getActivity(), recipe);
    }
}
