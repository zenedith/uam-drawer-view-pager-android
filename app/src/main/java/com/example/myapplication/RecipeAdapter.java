package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Exercise 2.
 *
 * Use RecipeDatabase instead of RecipesProvider.
 */
public class RecipeAdapter extends BaseAdapter {

    private RecipesProvider recipesProvider;
    private Context context;

    public RecipeAdapter(Context context) {
        this.context = context;
        this.recipesProvider = new SimpleRecipesProvider(context);
//        this.recipesProvider = new RecipeDatabase(context);
    }

    @Override
    public int getCount() {
        return recipesProvider.getRecipesNumber();
    }

    @Override
    public Recipe getItem(int position) {
        return recipesProvider.getRecipe(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View recipeView;

        if (convertView == null) {
            recipeView = LayoutInflater.from(context).inflate(R.layout.recipe_row, parent, false);
        } else {
            recipeView = convertView;
        }

        bindRecipeToView(getItem(position), recipeView, position);

        return recipeView;
    }

    private void bindRecipeToView(Recipe recipe, View recipeView, int position) {
        ImageView recipePhoto = (ImageView) recipeView.findViewById(R.id.recipe_photo);
        recipePhoto.setImageResource(getImageForPosition(position));

        TextView recipeLabel = (TextView) recipeView.findViewById(R.id.recipe_label);
        recipeLabel.setText(recipe.getDishName());

        TextView recipeRating = (TextView) recipeView.findViewById(R.id.recipe_rating);
        String recipeRatingText = recipe.getRating() + "/10";
        recipeRating.setText(recipeRatingText);
    }

    private int getImageForPosition(int position) {
        Integer[] imageRes = {
                R.drawable.recipe1,
                R.drawable.recipe2,
                R.drawable.recipe3,
                R.drawable.recipe4,
                R.drawable.recipe5,
                R.drawable.recipe6,
                R.drawable.recipe7,
                R.drawable.recipe8,
                R.drawable.recipe9,
                R.drawable.recipe10
        };

        int imageForPos = position % 10;

        return imageRes[imageForPos];
    }
}
