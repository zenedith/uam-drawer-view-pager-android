package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

public class RecipeDetailsActivity extends RecipesBaseActionbarActivity {

    public static final String RECIPE_EXTRA_KEY = "recipe";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        Intent i = getIntent();
        Recipe recipe = (Recipe) i.getExtras().getSerializable(RECIPE_EXTRA_KEY);

        RecipeDetailsFragment recipeDetailsFragment = new RecipeDetailsFragment();
        recipeDetailsFragment.setRecipe(recipe);

        addFragment(recipeDetailsFragment, R.id.container);
    }
}
