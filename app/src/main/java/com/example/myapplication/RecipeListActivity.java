package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

/**
 * @author Piotr Makowski (<a href=\"mailto:Piotr.Makowski@allegrogroup.pl\">Piotr.Makowski@allegrogroup.pl</a>)
 */
public class RecipeListActivity extends RecipesBaseActionbarActivity
        implements RecipeListFragment.OnShowRecipeActionListener {

    private RecipeDetailsFragment recipeDetailsFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        recipeDetailsFragment = new RecipeDetailsFragment();

        if (isTablet()) {
            addFragment(recipeDetailsFragment, R.id.details);
        }

        RecipeListFragment recipeListFragment = new RecipeListFragment();

        addFragment(recipeListFragment, R.id.container);
    }

    @Override
    public void showRecipe(Recipe recipe) {
        if (isTablet()) {
            recipeDetailsFragment.showRecipe(recipe);
        } else {
            Intent i = new Intent(this, RecipeDetailsActivity.class);

            i.putExtra(RecipeDetailsActivity.RECIPE_EXTRA_KEY, recipe);

            startActivity(i);
        }
    }
}
