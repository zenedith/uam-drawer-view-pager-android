package com.example.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

public abstract class RecipesBaseActionbarActivity extends ActionBarActivity {

    protected void addFragment(Fragment fragment, int container) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(container, fragment);
        fragmentTransaction.commit();
    }

    protected boolean isTablet() {
        return getResources().getBoolean(R.bool.isTablet);
    }
}
