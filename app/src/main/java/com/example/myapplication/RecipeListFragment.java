package com.example.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

public class RecipeListFragment extends Fragment {

    private OnShowRecipeActionListener listener;

    public interface OnShowRecipeActionListener {
        void showRecipe(Recipe recipe);
    }

    private ColumnNumberPreferences columnNumberPreferences;
    private GridView grid;

    public RecipeListFragment() {
        //nop
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            listener = (OnShowRecipeActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                    " must implement OnShowRecipeActionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recipe_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        columnNumberPreferences = new ColumnNumberPreferences(getActivity());

        initializeGrid();
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter.notifyDataSetChanged();
    }

    private void initializeGrid() {

        grid = (GridView) getView().findViewById(R.id.recipe_grid);

        adapter = new RecipeAdapter(getActivity());

        grid.setNumColumns(columnNumberPreferences.getSelectedNumberOfColumns());

        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Recipe recipe = adapter.getItem(position);
                listener.showRecipe(recipe);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.col_number: {
                showColNumberChooserDialog();
                return true;
            }
            case R.id.add_recipe: {
                startAddRecipeActivity();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }

    }

    private void startAddRecipeActivity() {
        Intent i = new Intent(getActivity(), AddRecipeActivity.class);
        startActivity(i);
    }

    private void showColNumberChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        List<CharSequence> options = new ArrayList<>();

        options.add("1");
        options.add("2");
        options.add("3");
        options.add("4");
        options.add("5");

        builder.setItems(options.toArray(new CharSequence[5]), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                setNumberOfGridColumns(which + 1);
            }
        });

        builder.setTitle("Select number of columns");

        builder.create().show();
    }

    private void setNumberOfGridColumns(int numberOfColumns) {
        grid.setNumColumns(numberOfColumns);
        columnNumberPreferences.saveSelectedNumberOfColumns(numberOfColumns);
    }

    private RecipeAdapter adapter;
}
